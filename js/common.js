(function($) {
	'use strict';

	var project = {};

	project.focusLink = function() { 
	    var a = new Date,
	        b = new Date;
	    $(document).on("focusin", function(c) {
	        $(".keyboard-outline").removeClass("keyboard-outline");
	        var d = b < a;
	        d && $(c.target).addClass("keyboard-outline");
	    }), $(document).on("click", function() {
	        b = new Date;
	    }), $(document).on("keydown", function() {
	        a = new Date;
	    });

	    var $oldButton = $('#mod_login').find('[name=loginUser]');
	    $oldButton.wrap('<div class="button-wrapper"></div>');
	    $('#mod_login').find('.button-wrapper').html('<button type="submit" value="' + $oldButton.attr('value') + '" name="' + $oldButton.attr('name') + '" class="' + $oldButton.attr('class') + '">' + $oldButton.attr('value'));
	    
	    var $oldButton = $('#mod_questionnaire').find('[name=vote]');
	    $oldButton.wrap('<div class="button-wrapper"></div>');
	    $('#mod_questionnaire').find('.button-wrapper').html('<button type="submit" value="' + $oldButton.attr('value') + '" name="' + $oldButton.attr('name') + '" class="' + $oldButton.attr('class') + '">' + $oldButton.attr('value') + '</button>');

	    var $oldButton = $('[name=loginUser]');
	    $oldButton.wrap('<div class="button-wrapper"></div>');
	    $('#mod_login').find('.button-wrapper').html('<button type="submit" value="' + $oldButton.attr('value') + '" name="' + $oldButton.attr('name') + '" class="' + $oldButton.attr('class') + '">' + $oldButton.attr('value'));
	    
	}

	project.calendar = function() {
		$(document).on("click", ".caption_nav_prev a", function()
		{ 
		    var date = $(this).attr('id').substr(2);
		    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

		    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
		});

		$(document).on("click", ".caption_nav_next a", function()
		{ 
		    var date = $(this).attr('id').substr(2);
		    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

		    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);		
		});
	}

	project.articles = function() {
		$('.article__image img').each(function() {
			var width = $(this).width();
			var height = $(this).height();
			$(this).parent('div').css({'width': width, 'height': height});
		});
	}

	project.specialModules = function() {
		if ($('.aside').has('#mod_contact')) {
        	$('.aside #mod_contact').clone().appendTo('#modulesTop2 > ul').wrap('<li class="modulesLeftMain"></li>');
		}
		if ($('.aside').has('#mod_forum')) {
        	$('.aside #mod_forum').clone().appendTo('#modulesTop2 > ul').wrap('<li class="modulesLeftMain"></li>');
		}
	}

	project.triggerUpload = function() {
		$('#btnFilePos').on('click', function() {
			$('#avatar_f').trigger('click');
		});
	}

	project.menu = function() {
		$('.nav__menu > ul ul').closest('.nav__menu > ul > li').children('a').addClass('menu-holder');
		$('.nav--hamburger').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
			$('.nav__menu').toggleClass('active');
		});
		$('.nav__menu .menu-holder').on('click', function(e) {
	        $('.nav__menu > ul > li ul').removeClass('active');
			if (!$(this).hasClass('ready')) {
		        $('.menu-holder').removeClass('ready');
				$(this).addClass('ready');
				$(this).siblings('ul').addClass('active');
		        e.preventDefault();
		    } 
		    else {
		        var href = $(this).attr('href');
		        window.location.href = href;
		    }
		});
		$(document).on('click', function(e) {
			var target = e.target;
			if (!$(target).is('.menu-holder')) {
				$('.nav__menu > ul > li ul').removeClass('active');
				$('.menu-holder.ready').removeClass('ready');
			}
		});
	}

	project.menuLeft = function() {
		$('.aside__menu ul[role="menu"]').siblings('a').addClass('not-ready');
		$('body').on('click','.not-ready','click', function(e) {
			e.preventDefault();
			$(this).addClass('ready').removeClass('not-ready');
			$(this).siblings('ul').show();
		});
		
		var parent = $('.aside__menu a.selected').closest('#mg > li');
		var parentDropdowns = $(parent).find('.dropdown-menu');
		$(parentDropdowns).show();
	}

	project.search = function() {
		$('.nav__search').on('click', function() {
			$(this).addClass('active');
		});
	}

	project.fancybox = function() {
		$('.photoWrapperGallery a').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	300, 
			'speedOut'		:	300, 
			'overlayShow'	:	true
		});
		$('.article__image').fancybox({
			'transitionIn'	:	'elastic',
			'transitionOut'	:	'elastic',
			'speedIn'		:	300, 
			'speedOut'		:	300, 
			'overlayShow'	:	true
		});
		if (popup.show) {
			$.fancybox(popup.content);	
	    }
	}

	project.sameHeightHeadmodules = function() {
		if (Modernizr.mq('(min-width: 768px)')) {
			var headmodulesHeading = 0;
			$('.headmodules__holder > li h2').each(function() {
				if ($(this).height() > headmodulesHeading) {
					headmodulesHeading = $(this).height();
				}
			});
			$('.headmodules__holder > li h2').height(headmodulesHeading);
		}
	}

	project.comeBack = function() {
		$('#goTop').on('click', function(e) {
			e.preventDefault();
			$('html,body').animate({ scrollTop: 0 }, 'slow');
        	return false; 
		});
	}

	project.fontSize = function() {
		if (Modernizr.mq('(max-width: 991px)')) {
			$('html').css({'font-size':'62.5%'});
		}
	}

	project.wrapper = function() {
		$('input[type="submit"], input[type="button"], button[type="submit"]').not('.nav__search button[type="submit"], [name="loginUser"], [name="vote"]').wrap('<div class="button-wrapper"></div>')
	}

	project.modulesMain = function() {
		$('.modules-main > ul > li').on('click', function() {
			if ($(this).find('a').length > 0) {
				var href = $(this).find('a').attr('href');
				window.location.href = href;
			}
		});
		$('.modules-main').each(function() {
			if ($(this).children('ul').children('li:not(.no-count)').length < 2) {
				$(this).addClass('single');
			}
			else if ($(this).children('ul').children('li:not(.no-count)').size() < 3) {
				$(this).addClass('twin');
			}
		})
	}

	project.weather = function() {
		$('.weatherParam').clone().appendTo('#mod_weather .module_content #weatherInfo').addClass('cloned');
	}

	var calculationDone = false;

	project.sameHeightAsideModules = function() {
		if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {
			var asideModules = 0;
			if (!calculationDone) {
				$('#modulesLeftWrapper > div').each(function() {
					if ($(this).height() > asideModules) {
						asideModules = $(this).height();
					}
				});
				$('#modulesLeftWrapper > div').css({'height': asideModules});
				calculationDone = true;
			}
		}
	}

	project.questionnaire = function() {
		$('.qBar').wrap('<div class="question-bar-wrapper" style="width: calc(100% - 80px);"></div>')
	}

	var isMobileView = false;

	project.startUI = function() {
		$('#advertsTopWrapper').wrap('<div id="advertsTopWrapper-desktop"></div>');
        $('#content_txt').wrap('<div id="content_txt-desktop"></div>');
    	$('.aside__menu').wrap('<div id="menuLeft-desktop"></div>');
        $('.headmodules').wrap('<div id="headmodules-desktop"></div>');
        $('#modulesTop2').wrap('<div id="modulesMainTop-desktop"></div>');
        $('#modulesLeftWrapper').wrap('<div id="modulesLeftWrapper-desktop"></div>');
	}

	project.updateUI = function() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            $('#advertsTopWrapper').prependTo('#advertsTopWrapper-mobile');
            $('#content_txt').prependTo('#content_txt-mobile');
            $('.aside__menu').prependTo('#menuLeft-mobile');
            $('.headmodules').prependTo('#headmodules-mobile');
            $('#modulesTop2').prependTo('#modulesMainTop-mobile');
            
            $('#modulesLeftWrapper').prependTo('#modulesLeftWrapper-mobile');
            $('#mod_programs').appendTo('#modPrograms-mobile .aside__modules');
        } 
        else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('#advertsTopWrapper').prependTo('#advertsTopWrapper-desktop');
            $('#content_txt').prependTo('#content_txt-desktop');
            $('.aside__menu').prependTo('#menuLeft-desktop');
            $('.headmodules').prependTo('#headmodules-desktop');
            $('#modulesTop2').prependTo('#modulesMainTop-desktop');
            
            $('#modulesLeftWrapper').prependTo('#modulesLeftWrapper-desktop');
            $('#mod_programs').appendTo('#modulesLeftWrapper-desktop .aside__modules');
        }
    }

	project.init = function() {
		project.focusLink();
		project.calendar();
		project.triggerUpload();
		project.menu();
		project.menuLeft();
		project.search();
		project.fancybox();
		project.comeBack();
		project.wrapper();
		project.modulesMain();
		project.weather();
		project.questionnaire();
		project.startUI();
		project.articles();
	}

	$(window).on('load', function() {
		project.updateUI();
		project.specialModules();
		project.sameHeightHeadmodules();
		project.sameHeightAsideModules();
		project.fontSize();
	});

	$(window).on('resize', function() {
		project.updateUI();
		project.sameHeightHeadmodules();
		project.sameHeightAsideModules();
		project.fontSize();
		project.articles();
	});

	$(document).ready(project.init);

})(jQuery);