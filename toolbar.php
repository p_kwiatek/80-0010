<nav class="main-nav">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 sm-visible xs-visible">
				<button class="nav--hamburger" type="button">
                    <i class="icon-menu icon-top icon" aria-hidden="true"></i>
                    <i class="icon-menu icon-middle icon" aria-hidden="true"></i>
                    <i class="icon-menu icon-bottom icon" aria-hidden="true"></i>
			        <span class="sr-only"><?php echo __('expand')?></span>
			        <span class="title sr-only"><?php echo __('main menu')?></span>
                </button>
			</div>
			<div class="col-xs-12 nav__content--holder">
				<?php
					$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
					echo '<a href="index.php" class="nav__home"><img src="'. $pathTemplate .'/images/nav/home.png" alt="' . __('home page') . '"></a>' . "\r\n";
				?>
				<div class="nav__content">
					<?php
						$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
						echo '<a href="index.php" class="nav__home--mobile"><img src="'. $pathTemplate .'/images/nav/home-mobile.png" alt="' . __('home page') . '"></a>' . "\r\n";
					?>
					<?php
						$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
						echo '<a href="'. $pageInfo['bip'] .'" class="nav__bip"><img src="'. $pathTemplate .'/images/nav/bip-color1.png" class="hidden-mobile" alt="'. __('bip') .'"><img src="'. $pathTemplate .'/images/nav/bip-color2.png" class="visible-mobile" alt="'. __('bip') .'"></a>' . "\r\n";
					?>
					<div class="nav__content--mobile">
						<?php
							$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
						    if ($_SESSION['contr'] == 0)
						    {
							$set_contrast = 1;
							$contrast_txt = __('contrast version');
						    } else
						    {
							$set_contrast = 0;
							$contrast_txt = __('graphic version');
						    }
							echo '<a href="ch_style.php?contr='. $set_contrast .'" class="nav__contrast">' . __('font contrast') .': <img src="'. $pathTemplate .'/images/nav/contrast-color1.png" alt="' . $contrast_txt .'" class="hidden-mobile"><img src="'. $pathTemplate .'/images/nav/contrast-color2.png" alt="' . $contrast_txt .'" class="visible-mobile"><span class="sr-only">' . $contrast_txt . '</span></a>' . "\r\n";
						?>
						<ul class="nav__fonts">
						    <li><a href="ch_style.php?style=0" class="medium" title="<?php echo __('font default'); ?>"><span aria-hidden="true">A<span class="sr-only"><?php echo __('font default'); ?></span></a></li>
						    <li><a href="ch_style.php?style=r1" class="big" title="<?php echo __('font bigger'); ?>"><span aria-hidden="true">A<sup>+</sup></span><span class="sr-only"><?php echo __('font bigger'); ?></span></a></li>
						    <li><a href="ch_style.php?style=r2" class="large" title="<?php echo __('font biggest') ?>"><span aria-hidden="true">A<sup>++</sup></span><span class="sr-only"><?php echo __('font biggest'); ?></span></a></li>
						</ul>
						<div class="nav__search">
							<form id="searchForm" name="f_szukaj" method="get" action="index.php">
		                		<input name="c" type="hidden" value="search" />
		                		<a name="skip_srch" id="skip_srch"><span class="sr-only"><?php echo __('search'); ?></span></a>
		                		<label for="kword"  class="sr-only"><?php echo __('search query'); ?>:</label>
								<input type="text" id="kword" name="kword" size="24" maxlength="40" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
					            <button type="submit" name="search"><?php echo __('search action'); ?></button>
					        </form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="nav__menu">
					<a name="skip_tm" id="skip_tm"></a>
					<?php get_menu_tree('tm', 0, 0, '', false, '', false, false, false, true, true); ?>
				</div>
			</div>
		</div>
	</div>
</nav>