<div id="popup"></div>
<ul class="skipLinks">
    <li><a href="#skip_tm"><?php echo __('skip to main menu')?></a></li> 
    <li><a href="#skip_mg"><?php echo __('skip to additional menu')?></a></li> 
    <li><a href="#content_txt"><?php echo __('skip to content')?></a></li>
    <li><a href="#skip_srch"><?php echo __('skip to search')?></a></li>
    <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
</ul>
<?php
include_once ( CMS_TEMPL . DS . 'toolbar.php');
?>
<header class="hero">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
            	<div class="hero__content">
            		<h1>
            			<?php echo word_wrap( array(' w', ' im'), $pageInfo['name']); ?>	
        			</h1>
        			<div>
						<?php
						    echo $headerAddress;
						    if ($pageInfo['email'] != '')
						    {
							echo '<a href="mailto:'.$pageInfo['email'].'">'.$pageInfo['email'].'</a>';
						    }	    
					    ?>
					</div>
				</div>
				<div class="hero__objects">
				    <?php
				    	$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
						echo '<img src="'. $pathTemplate .'/images/hero/clouds.png" alt="" class="hero__clouds">' . "\r\n"; 
						echo '<img src="'. $pathTemplate .'/images/hero/objects-right.png" alt="" class="hero__objects--right">' . "\r\n"; 
						echo '<img src="'. $pathTemplate .'/images/hero/objects-left.png" alt="" class="hero__objects--left">' . "\r\n"; 
						echo '<img src="'. $pathTemplate .'/images/hero/objects-person.png" alt="" class="hero__objects--person">' . "\r\n"; 
				    ?>
				</div>
				<div class="hero__carousel">
                    <div>
                    	<?php if ($outBannerTopRows == 0): ?>
                            <?php
                                $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                $pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
                        	    echo '<div class="hero__background" style="background-image: url('. $img .');"></div>';
                	        ?>       
                    	<?php elseif ($outBannerTopRows == 1): ?>
                    	    <?php
                        	    $value = $outBannerTop[0];
                        	    $pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
                        	    $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . rawurlencode($value['photo']);
                        	    echo '<div class="hero__background" style="background-image: url('. $img .');"></div>';
                	        ?>
                    	<?php else: ?>
                    	    <?php foreach ($outBannerTop as $value): ?>
                    	        <?php
                        	        $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . rawurlencode($value['photo']);
                        	        echo '<div class="hero__background" style="background-image:url('. $img .');"></div>';
                    	        ?>
                    	    <?php endforeach ?>
                    	<?php endif ?>
                    </div>
                </div>
        	    <div class="hero__terrain"></div>
		    </div>
        </div>
	</div>
</header>
