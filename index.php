<?php
include_once ( CMS_TEMPL . DS . 'i18n' . DS . $lang . '.php');
include_once ( CMS_TEMPL . DS . 'header.php');		
include_once ( CMS_TEMPL . DS . 'top.php');
?>
	<?php
	include_once('modules_top.php');
	?>	    
	<section class="main">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-md-3 hidden-xs hidden-sm">
					<?php
						include_once ( CMS_TEMPL . DS . 'left.php');
					?>
				</div>
				<div class="col-md-9">
					<?php
						include_once('modules_top2.php');
					?>	  
					<a name="skip_txt" id="skip_txt"></a>
				    <?php
				    include_once( CMS_TEMPL . DS . 'topAdv.php');
				    ?>
				    <div id="crumbpath"><span class="here">Jesteś tutaj:</span> <? echo show_crumbpath($crumbpath, $crumbpathSep); ?></div>
				    <div id="content_txt">
					<?php
					    include_once ( $TEMPL_PATH );	
					?>
				    </div>
					<div id="content_txt-mobile"></div>
					<div id="menuLeft-mobile" class="aside"></div>
					<div id="headmodules-mobile"></div>
					<div id="modulesMainTop-mobile"></div>
					<div id="modulesLeftWrapper-mobile" class="aside"></div>
					<?php
						include_once('modules_bottom.php');
					?>
					<div id="modPrograms-mobile" class="aside"><div class="aside__modules"></div></div>
				</div>
			</div>
		    <?php
			    include_once ( CMS_TEMPL . DS . 'bottom.php');
		    ?>
	    </div>    
	 </section>    
<?php
include_once ( CMS_TEMPL . DS . 'footer.php');		
?>
