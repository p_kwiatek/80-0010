<?php
if (count($topAdv) > 0)
{
    ?>
    <div id="advertsTopWrapper">
    <?php
    $n = 0;
    foreach ($topAdv as $adv)
    {
	?>
	<div class="advertTop">
	<?php
	$extUrl = '';
	if ($adv['ext_url'] != '')
	{
	    $newWindow = '';
	    if ($adv['new_window'] == 1)
	    {
		$newWindow = ' target="_blank" ';
	    }
		
	    $swfLink = '';
	    $swfSize = '';
	    if ($adv['attrib'] != '' && $adv['type'] == 'flash')
	    {
		$swfSize = explode(',', $adv['attrib']);
		$swfLink = ' style="width:'.$swfSize[0].'px; height:'.$swfSize[1].'px; z-index:1; display:block; position:absolute; left:0; top:0" ';
		}
		
		
		$extUrl = '<a href="'.$adv['ext_url'].'"'.$newWindow.$swfLink.'>';
		$extUrlEnd = '</a>';
	    }
	    
	    switch ($adv['type']){
		case 'flash':
		    $swfSize = explode(',', $adv['attrib']);
		    //echo $extUrl;
		    ?>
		    <div id="advTop_<?php echo $n?>"></div>
		    <script type="text/javascript">
		    // <![CDATA['
			swfobject.embedSWF("<?php echo $adv['content'] . '?noc='.time()?>", "advTop_<?php echo $n?>", "<?php echo $swfSize[0]?>", "<?php echo $swfSize[1]?>", "9.0.0", "expressInstall.swf", {}, {wmode:"transparent"}, {});
		    // ]]>
		    </script>
		    <?php
		    //echo $extUrlEnd;
		    break;
		
		case 'html':
		    echo $adv['content'];
		    break;
		
		case 'image':
		default:
		    $width == '';
		    if ($adv['content'] != '')
		    {                       
                        $size = getimagesize($adv['content']);
                        if ($size[0] > $templateConfig['maxWidthTopAdv']){
                            $width = ' width="' . $templateConfig['maxWidthLeftAdv'] . '" ';
                        }
                        echo $extUrl;
                        ?>
                        <img src="<?php echo $adv['content']?>" alt="<?php echo $adv['name']?>" <?php echo $width?>/>
                        <?php
                        echo $extUrlEnd;
		    }
		    break;
	    }
	?>
	</div>
	<?php
	$n++;
    }
    ?>
    </div>
    <?php
}
?>
