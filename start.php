<h2 class="heading"><?php echo __('home page'); ?></h2>
<?php
if ($showPopup && $_COOKIE['popupShow'] != 1) 
{
?>
<script>
// <![CDATA[
popup = {
    show: true,
    content: '<?php $order = array("\r\n", "\n", "\r"); $replace = ''; echo str_replace($order, $replace, $popup['text']); ?>',
    width: <?php if ($popup['width'] == 0) echo "'auto'"; else echo $popup['width']; ?>,
    height: <?php if ($popup['height'] == 0) echo "'auto'"; else echo $popup['height']; ?>,
    popupBackground: '<?php echo $popupBackground?>'
};
// ]]>
</script>

<?php
}
echo $message;

/*
 * Tekst powitalny
 */
if ($showWelcome)
{
    ?>
    <div id="welcome"><?php echo $txtWelcome?></div>
    <?php
}  

/*
 * Tablica
 */
if ($showBoard)
{
?>

<div id="board" class="board">
    <div class="board-top">
        <div class="board-top-left"></div>
        <div class="board-top-top"></div>
        <div class="board-top-right"></div>
    </div>
    <div class="board-center">
        <div class="board-center-left"></div>
        <div class="board-center-center">
            <div class="board-content">
                <h3 class="board-header"><?php echo __('board info')?></h3>
                <?php echo $txtBoard; ?>
            </div>
        </div>
        <div class="board-center-right"></div>
    </div>
    <div class="board-bottom">
        <div class="board-bottom-left"></div>
        <div class="board-bottom-bottom"></div>
        <div class="board-bottom-right"></div>
        <div class="chalk-1"></div>
        <div class="chalk-2"></div>
        <div class="sponge"></div>
    </div>    
</div>


<?php
}

/*
 * Articles
 */
if ($artCounter > 0)
{
    ?>
    <section class="articles">
	<h3 class="heading"><?php echo __('articles'); ?></h3>
	<?php
	$i = 0;
	foreach ($outRowArticles as $row)
	{
	    $highlight = $url = $target = $url_title = $protect = '';

	    if ($row['protected'] == 1)
	    {
		$protect = '<span class="protectedPage"></span>';
		$url_title = ' title="' . __('page requires login') . '"';
	    }				

	    if (trim($row['ext_url']) != '')
	    {
		if ($row['new_window'] == '1')
		{
		    $target = ' target="_blank"';
		}	
		$url_title = ' title="' . __('opens in new window') . '"';
		$url = ref_replace($row['ext_url']);					
	    } else
	    {
		if ($row['url_name'] != '')
		{
		    $url = 'a,' . $row['id_art'] . ',' . $row['url_name'];
		} else
		{
		    $url = 'index.php?c=article&amp;id=' . $row['id_art'];
		}
	    }	

	    $margin = ' noPhoto';
	    if (is_array($photoLead[$row['id_art']]))
	    {
		$margin = '';
	    }
	    
	    $row['show_date'] = substr($row['show_date'], 0, 10);

	    $highlight = '';
	    $frame = 'normalFrame';
	    $photoWrapper = '';
	    if ($row['highlight'] == 1)
	    {
		$highlight = ' article--highlighted';
		$frame = 'highlightFrame';
		$photoWrapper = ' highlightWrapper';
	    }	    
	    ?>
	    <div class="article<?php echo $highlight?>">
   		<?php
		$marginPhoto = '';
		if (is_array($photoLead[$row['id_art']]))
		{
		    $photo = $photoLead[$row['id_art']];
		    $marginPhoto = ' paddingBottom';
		    ?>
			<a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" class="article__image">
				<div><img src="files/<?php echo $lang?>/mini/<?php echo $photo['file']?>" alt="<?php echo __('enlarge image'); ?>"></div>
			</a>
		    <?php
		}
		?>
			<div class="article__content">
				<h4><a href="<?php echo $url?>" <?php echo $url_title . $target?>><?php echo $row['name'] . $protect ?></a></h4>
				<?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
				    <div class="article__date"><span><?php echo $row['show_date']?></span></div>
				<?php } ?>
			    <div><?php echo $row['lead_text']?></div>
			    <a href="<?php echo $url?>" <?php echo $url_title . $target?> class="article__link"><?php echo __('read more'); ?><span class="sr-only"> <?php echo __('about'); ?>: <?php echo $row['name'] . $protect?></span></a>
			</div>	    
	    </div>
	    <?php
	}
	$url = $PHP_SELF.'?c=' . $_GET['c'] . '&amp;id=' . $_GET['id'] . '&amp;s=';
	include (CMS_TEMPL . DS . 'pagination.php');
	?>
    </section>
    <?php
}

